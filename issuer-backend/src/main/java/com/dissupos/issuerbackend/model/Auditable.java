package com.dissupos.issuerbackend.model;

import com.dissupos.issuerbackend.config.IssuerAuditorAware;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(IssuerAuditorAware.class)
@NoArgsConstructor
public abstract class Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;
    @CreatedDate
    protected LocalDateTime createdAt;
    @LastModifiedDate
    protected LocalDateTime updatedAt;
    @CreatedBy
    protected String createdBy;
    @LastModifiedBy
    protected String updatedBy;
}
