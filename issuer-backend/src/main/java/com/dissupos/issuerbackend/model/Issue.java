package com.dissupos.issuerbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Issue extends Auditable {
    private String name;
    private String description;
    @JsonIgnore()
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;
}
