package com.dissupos.issuerbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Project extends Auditable {
    private String name;
    private String description;
    @JsonIgnore
    @OneToMany(mappedBy = "project")
    private List<Issue> issues;

    @JsonProperty
    public int issuesCount() {
        return issues.size();
    }
}
