package com.dissupos.issuerbackend.repository;

import com.dissupos.issuerbackend.model.Issue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IssueRepository extends PagingAndSortingRepository<Issue, Long>, JpaSpecificationExecutor<Issue> {
    Page<Issue> findAllByProjectId(Long projectId, Pageable pageable);
}
