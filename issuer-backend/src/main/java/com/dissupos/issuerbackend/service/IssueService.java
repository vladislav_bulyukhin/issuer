package com.dissupos.issuerbackend.service;

import com.dissupos.issuerbackend.model.Issue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface IssueService {
    Page<Issue> getAllIssuesByProjectId(Long projectId, Pageable pageable);
}
