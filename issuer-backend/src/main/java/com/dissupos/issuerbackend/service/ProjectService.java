package com.dissupos.issuerbackend.service;

import com.dissupos.issuerbackend.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface ProjectService {
    Page<Project> findPaginated(Pageable page);

    Project save(Project project);
}
