package com.dissupos.issuerbackend.service.impl;

import com.dissupos.issuerbackend.model.Issue;
import com.dissupos.issuerbackend.repository.IssueRepository;
import com.dissupos.issuerbackend.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class IssueServiceImpl implements IssueService {

    private IssueRepository repository;

    @Autowired
    public IssueServiceImpl(IssueRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<Issue> getAllIssuesByProjectId(Long projectId, Pageable pageable) {
        return repository.findAllByProjectId(projectId, pageable);
    }
}
