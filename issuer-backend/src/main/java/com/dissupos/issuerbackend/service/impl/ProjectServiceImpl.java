package com.dissupos.issuerbackend.service.impl;

import com.dissupos.issuerbackend.model.Project;
import com.dissupos.issuerbackend.repository.ProjectRepository;
import com.dissupos.issuerbackend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository repository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<Project> findPaginated(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Project save(Project project) {
        return repository.save(project);
    }
}
