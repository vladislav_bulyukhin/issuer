package com.dissupos.issuerbackend.web.resource;

import com.dissupos.issuerbackend.model.Issue;
import com.dissupos.issuerbackend.service.IssueService;
import com.dissupos.issuerbackend.utils.annotation.RestApiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestApiController
public class IssueResource {

    private IssueService service;

    @Autowired
    public IssueResource(IssueService service) {
        this.service = service;
    }

    @CrossOrigin(origins = "http://127.0.0.1:8080")
    @GetMapping("/projects/{projectId}/issues")
    public ResponseEntity<Page<Issue>> getIssuesByProjectId(@PathVariable Long projectId, Pageable pageable) {
        return ResponseEntity.ok(service.getAllIssuesByProjectId(projectId, pageable));
    }
}
