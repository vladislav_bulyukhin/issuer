package com.dissupos.issuerbackend.web.resource;

import com.dissupos.issuerbackend.model.Project;
import com.dissupos.issuerbackend.service.ProjectService;
import com.dissupos.issuerbackend.utils.annotation.RestApiController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestApiController
@Slf4j
public class ProjectResource {
    private ProjectService service;

    @Autowired
    public ProjectResource(ProjectService service) {
        this.service = service;
    }

    @CrossOrigin(origins = "http://127.0.0.1:8080")
    @GetMapping(value = "/projects")
    public ResponseEntity<Page<Project>> getProjects(Pageable pageable) {
        log.debug("get all projects");
        Page<Project> projectPage = service.findPaginated(pageable);
        return ResponseEntity.ok(projectPage);
    }

    @PostMapping("/projects")
    public void createProject() {
        log.debug("create project");

    }

    @PutMapping("/projects")
    public void updateProject() {
        log.debug("update project");

    }

    @DeleteMapping("/projects")
    public void deleteProject() {
        log.debug("delete project");
    }

    @GetMapping("/projects/{id}")
    public void getProjectById(@PathVariable Long id) {
        log.debug("get project by id");
    }
}
