import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import ProjectTable from '@/components/ProjectTable'
import Search from '@/components/Search'
import UserProfile from '@/components/UserProfile'
import Login from '@/components/Login'
import IssueTable from '@/components/IssueTable'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      component: Login
    },
    {
      path: '/',
      component: Index,
      children: [
        {path: '', component: ProjectTable},
        {path: 'search', component: Search},
        {path: 'user/:id', component: UserProfile},
        {path: 'projects/:id/issues', component: IssueTable, props: true}
      ]
    }
  ]
})
