import Vue from 'vue'
import Vuex from 'vuex'
import Auth from '@/store/module/Auth'
import Project from '@/store/module/Project'
import Issue from '@/store/module/Issue'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: Auth,
    project: Project,
    issue: Issue
  }
})
