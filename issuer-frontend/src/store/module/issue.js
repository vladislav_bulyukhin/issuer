import axios from 'axios'

export default {
  state: {
    projectId: 0,
    issues: [],
    totalPages: 0,
    errors: []
  },
  mutations: {
    setIssues (state, issues) {
      state.issues = issues
    },
    setTotalPages (state, totalPages) {
      state.totalPages = totalPages
    },
    setProjectId (state, projectId) {
      state.projectId = projectId
    }
  },
  actions: {
    getIssueByPage ({commit}, {projectId, page}) {
      axios.get(`http://127.0.0.1:8090/api/projects/${projectId}/issues`, {
        params: {
          page: page,
          size: 25
        }
      }).then(
        response => {
          console.log(projectId)
          const issues = response.data.content
          const totalPages = response.data.totalPages
          commit('setIssues', issues)
          commit('setTotalPages', totalPages)
          commit('setProjectId', projectId)
        }
      ).catch(e => {
        console.log(e)
      })
    }
  }
}
