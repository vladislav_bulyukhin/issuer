import axios from 'axios'

export default {
  state: {
    projects: [],
    totalPages: 2,
    errors: []
  },
  mutations: {
    setProjects (state, projects) {
      state.projects = projects
    },
    setTotalPages (state, totalPages) {
      state.totalPages = totalPages
    },
  },
  actions: {
    getDateByPage ({commit}, page) {
      axios.get(`http://127.0.0.1:8090/api/projects`, {
        params: {
          page: page,
          size: 10
        }
      }).then(
        response => {
          const projects = response.data.content
          const totalPages = response.data.totalPages
          commit('setProjects', projects)
          commit('setTotalPages', totalPages)
        }
      ).catch(e => {
        console.log(e)
      })
    }
  }
}
